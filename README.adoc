= M4 HTML
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

- link:configuracio_git.adoc[Passos per a configurar el GIT]
- link:exemples_html.adoc[Exemples HTML]
- link:https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms[Formularis HTML]

Activitats:

- link:questions_html.adoc[Qüestions bàsiques sobre HTML]
